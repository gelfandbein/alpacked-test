#!/bin/sh

cd /tmp

MYSQL_USER=root
MYSQL_PASS=

ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

sed -i 's/#cron/cron/' /etc/rsyslog.conf
sed -i '/imklog/s/^/#/' /etc/rsyslog.conf
sed -i 's/*.*;auth,authpriv.none/*.*;auth,authpriv.none,cron.none/g' /etc/rsyslog.conf

### selected v4 becouse dump mysql db after import don't likes by zabbix-server
wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+focal_all.deb
dpkg -i zabbix-release_4.0-3+focal_all.deb
apt -y install zabbix-server-mysql zabbix-frontend-php zabbix-agent

### created file for zabbix db, user & grant privileges to him
touch /tmp/create.sql
echo "create database zabbix character set utf8 collate utf8_bin;\ncreate user zabbix@localhost identified by '123123123';\ngrant all privileges on zabbix.* to zabbix@localhost;" > /tmp/create.sql

### pushed commands to mysql server. automaticaly version
service mysql start
mysql --user=$MYSQL_USER --password="$MYSQL_PASS" < /tmp/create.sql

### selected v4 zabbix...
wget https://cdn.zabbix.com/zabbix/sources/stable/4.0/zabbix-4.0.22.tar.gz
tar -xf zabbix-4.0.22.tar.gz
cd zabbix-4.0.22/database/mysql

### import zabbix db dump to empty db
mysql -uzabbix -p123123123 zabbix < schema.sql
mysql -uzabbix -p123123123 zabbix < images.sql
mysql -uzabbix -p123123123 zabbix < data.sql

### timezone for PHP
ln -s /etc/apache2/conf-available/zabbix-frontend-php.conf /etc/apache2/conf-enabled/zabbix-frontend-php.conf
sed 's/;date.timezone =/date.timezone = Europe\/Kiev/g' /etc/php/7.4/apache2/php.ini > /etc/php/7.4/apache2/php.ini.new
mv /etc/php/7.4/apache2/php.ini.new /etc/php/7.4/apache2/php.ini

### password to DB for zabbix-server
sed 's/# DBPassword=/DBPassword=123123123/g' /etc/zabbix/zabbix_server.conf > /etc/zabbix/zabbix_server.conf.new
mv /etc/zabbix/zabbix_server.conf.new /etc/zabbix/zabbix_server.conf

### stop active checks to server by agent
sed 's/ServerActive=127.0.0.1/#ServerActive=127.0.0.1/g' /etc/zabbix/zabbix_agentd.conf > /etc/zabbix/zabbix_agentd.conf.new
mv /etc/zabbix/zabbix_agentd.conf.new /etc/zabbix/zabbix_agentd.conf

### put web conf to zabbix folder for easily web start without setup
mv /tmp/zabbix.conf.php /etc/zabbix
