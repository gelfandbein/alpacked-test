# Alpacked test

Alpacked test

Variables:
CI_REGISTRY_DOCKER   -> docker.io
CI_REGISTRY_IMAGE    -> gelfandbein/alpacked-test
CI_REGISTRY_USER     -> *******
CI_REGISTRY_PASSWORD -> *******

# ToDo

- сделать открытый репозиторий в gitlab в котором вы подготовите докерфайл и все необходимые к нему файлы.
- Докерфайл должен запускать zabbix сервер
- Докерфайл должен брать за основу ubuntu ( запрещается использовать готовый докерфайл )
- По каждому пушу в мастер бранчу должен тригериться GitlabCI который будет собирать докер image c этого репозитория, и пушить на ваш docker hub
- очень важно добавить комментарии в файлах которые делаете
- добавить Readme с небольшим описанием того что делалось, что не удалось сделать, и что осталось непонятным

# Changes

- create project at GitLab
- created Dockerfile & etc
- added variables to gitlab: CI_REGISTRY_DOCKER, CI_REGISTRY_IMAGE, CI_REGISTRY_USER & CI_REGISTRY_PASSWORD
- go to google to read how to push to docker registry
- solved policy-rc.d
- problem: with upload to hub.docker. pushing the old dockefile (image)
- problem: use the 'deploy' key, which will be ignored.
- zabbix:80 User: Admin , Password: zabbix / phpmyadmin:8888 User: root , Password: root_pwd
- dind...
- can't start docker in ubuntu container only in --privileged
- docker run --privileged -d docker:dind
- stop docker-in-docker, purge the project & reload all ;)
- can't run docker image in background. fixed by bash script :(
- done. web access at 80 port: login 'Admin' pass 'zabbix'. monitored himself
