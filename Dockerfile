FROM ubuntu:focal
MAINTAINER GBL <gbl@myblabla.name>

### non-interactive mode for installation
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Kiev

WORKDIR /tmp

### edit policy if needed to run packages after install
# RUN echo '#!/bin/sh' > /usr/sbin/policy-rc.d && echo 'exit 0' >> /usr/sbin/policy-rc.d && chmod +x /usr/sbin/policy-rc.d

### install main soft
RUN apt-get update && apt-get -y --no-install-recommends \
    install apt-utils gnupg-agent apt-transport-https ca-certificates curl software-properties-common sudo mc net-tools vim iptables wget \
    mysql-server apache2 php7.4-mysql rsyslog cron

### copy my files for zabbix full load
COPY ./tmp/install.sh .
COPY ./tmp/start.sh .
COPY ./tmp/zabbix.conf.php .

### run main installation script
RUN /tmp/install.sh

WORKDIR /

EXPOSE 80/tcp

ENTRYPOINT ["/tmp/start.sh", "-d"]

CMD ["tail", "-f", "/dev/null"]
